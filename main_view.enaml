""" app widgets """
import atom.api
import enaml
from enaml.core.api import Looper
from enaml.drag_drop import DragData
from enaml.layout.api import hbox, vbox
from enaml.styling import StyleSheet, Style, Setter
from enaml.widgets.html import Html
from enaml.widgets.api import Border, Feature, Field, ScrollArea, StatusBar, StatusItem

from ae.gui_app import flow_action, flow_key, id_of_flow, replace_flow_action
from ae.enaml_app.functions import enaml_rgba
with enaml.imports():
    from ae.enaml_app.widgets import FlowButton, FlowPopup, ThemeButton, ThemeField, ThemeMainWindow, ThemeContainer
from ae.lisz_app_data import FLOW_PATH_ROOT_ID, FLOW_PATH_TEXT_SEP  # used for to identify parent/child drop widgets


class ListItemAtom(atom.api.Atom):
    """ list item data atom, declared here because used by this enaml module as well as by main.py """
    index = atom.api.Int()

    id = atom.api.Str()
    sel = atom.api.Float()
    node = atom.api.Value()


enamldef Main(ThemeMainWindow): main_win:
    alias leave_node_button             #: button to go up to parent list (for to be disabled if reached the root list)
    alias scroller                      #: ScrollArea alias used for focus traversal within list items
    alias status_bar_                   #: main window status bar

    # STRANGE: adding typing hint `: LiszNode` to dragging_from_node results in run time error on drag start:
    # TypeError: Parameterized generics cannot be used with class or instance checks
    attr dragging_from_node             #: current_node_items list instance where drag item got dragged from
    attr drop_item_id: str              #: save drop item id workaround (does not work putting attr within drag_button)

    features = Feature.FocusTraversal
    previous_focus_child => (ewi):
        return main_app.focus_neighbour_sel_button('prev', ewi)
    next_focus_child => (ewi):
        return main_app.focus_neighbour_sel_button('next', ewi)

    ThemeContainer:
        constraints = [
            vbox(
                hbox(help_button, settings_button, leave_node_button, flow_path_button,
                     selected_filter_button, unselected_filter_button,
                     spacing=3, margins=3,
                    ),
                scroller, ),
        ]
        FlowButton: help_button:
            tap_flow_id = id_of_flow('show', 'help')
            icon_name = 'app_icon'
        FlowButton: settings_button:
            tap_flow_id = id_of_flow('open', 'user_preferences')
        FlowButton: leave_node_button:
            tap_flow_id = id_of_flow('leave', 'item')
            features = Feature.DropEnabled
            drag_enter => (event):
                if event.mime_data().has_format('text/plain'):
                    event.accept_proposed_action()
            drop => (event):
                main_win.drop_item_id = FLOW_PATH_ROOT_ID
            background << enaml_rgba(*app.app_state_flow_path_ink)
            visible << bool(app.app_state_flow_path) and main_app.flow_path_action() == 'enter'
            minimum_size << (0, app.app_state_font_size * 1.5)
        FlowButton: flow_path_button:
            tap_flow_id = id_of_flow('open', 'flow_path_jumper')
            text << main_app.flow_path_text(app.app_state_flow_path, min_len=6 if app.landscape else 3) \
                + main_app.flow_key_text(app.app_state_flow_id, app.landscape)
            background << enaml_rgba(*app.app_state_flow_path_ink)
            hug_width = 'ignore'
            status_tip = "display current list and item - click for to jump to a neighbour list"
            tool_tip = "display current list and item - click for to jump to a neighbour list"
        FlowButton: selected_filter_button:
            tap_flow_id << id_of_flow('toggle', 'filter', 'filter_selected')
            icon_name << 'filter_on' if app.app_state_filter_selected else 'filter_off'
            background << enaml_rgba(*app.app_state_selected_item_ink)
        FlowButton: unselected_filter_button:
            tap_flow_id << id_of_flow('toggle', 'filter', 'filter_unselected')
            icon_name << 'filter_on' if app.app_state_filter_unselected else 'filter_off'
            background << enaml_rgba(*app.app_state_unselected_item_ink)
        ScrollArea: scroller:
            horizontal_policy = 'always_off'
            ThemeContainer:
                Looper: looper:
                    iterable << app.filtered_atoms
                    ThemeContainer:
                        padding = 0
                        # does not exist as Container attributes but in constraints: spacing = 0 / margins = 0
                        constraints = [hbox(enter_item_button, sel_button, drag_button, spacing=0, margins=0)]
                        FlowButton: enter_item_button:
                            tap_flow_id << id_of_flow('enter', 'item', loop.item.id)
                            features = Feature.DropEnabled
                            drag_enter => (event):
                                if event.mime_data().has_format('text/plain'):
                                    event.accept_proposed_action()
                            drop => (event):
                                main_win.drop_item_id = FLOW_PATH_TEXT_SEP + loop.item.id
                            background << enaml_rgba(*app.app_state_flow_path_ink)
                            minimum_size << (0, app.app_state_font_size * 1.5)
                            visible << loop.item.node is not None
                        ThemeButton: sel_button:
                            # V 0.6.0: had to comment out the following 3 lines for to get clicked event working
                            #features = Feature.FocusEvents
                            #focus_gained => ():
                            #    main_app.focused_sel_button(loop.item.id)
                            StyleSheet:
                                Style:
                                    Setter:
                                        field = 'text-align'
                                        value = 'left'
                            text = loop.item.id
                            hug_width = 'ignore'
                            resist_width = 'ignore'
                            clicked :: main_app.change_flow(id_of_flow('toggle', 'item_sel', loop.item.id))
                            background << enaml_rgba(*(app.app_state_selected_item_ink if loop.item.sel else
                                                       app.app_state_unselected_item_ink))
                            font << str(int(app.app_state_font_size)) + 'px arial'
                            minimum_size << (int(app.app_state_font_size * 1.5), int(app.app_state_font_size * 1.5))
                            status_tip = f"de-/select item '{loop.item.id}'"
                            tool_tip = f"de-/select item '{loop.item.id}'"
                        FlowButton: drag_button:
                            tap_flow_id = id_of_flow('drag', 'item', loop.item.id)
                            features = Feature.DragEnabled | Feature.DropEnabled
                            drag_start => ():
                                main_win.dragging_from_node = main_app.current_node_items
                                drag_data = DragData()
                                drag_data.mime_data.set_data('text/plain', bytes(loop.item.id, 'utf-8'))
                                return drag_data
                            drag_end => (drag_data, result):
                                main_app.dropping_item(main_win.dragging_from_node,
                                                       drag_data.mime_data.data('text/plain').decode('utf-8'),
                                                       main_win.drop_item_id)
                            drag_enter => (event):
                                if event.mime_data().has_format('text/plain'):
                                    event.accept_proposed_action()
                            drop => (event):
                                main_win.drop_item_id = loop.item.id    # results in None w/o main_win
                            minimum_size << (0, app.app_state_font_size * 1.5)
                            visible << not app.app_state_filter_selected and not app.app_state_filter_unselected
    StatusBar: status_bar_:     # added trailing underscore because alias status_bar is raising an enaml TypeError
        font << str(int(app.app_state_font_size * 0.69)) + 'px arial'
        StatusItem:
            mode = 'permanent'
            FlowButton:
                tap_flow_id = id_of_flow('confirm', 'item_deletion', flow_key(app.app_state_flow_id))
                icon_name = id_of_flow('delete', 'item')
                visible << flow_action(app.app_state_flow_id) == 'focus'
        StatusItem:
            mode = 'permanent'
            FlowButton:
                tap_flow_id << replace_flow_action(app.app_state_flow_id, 'edit')
                visible << flow_action(app.app_state_flow_id) == 'focus'
        StatusItem:
            mode = 'permanent'
            FlowButton:
                tap_flow_id = id_of_flow('add', 'item')


enamldef ItemPopup(FlowPopup): edit_item_popup:
    font << str(int(app.app_state_font_size)) + 'px arial'
    ThemeContainer:
        border = Border()
        constraints = [vbox(hbox(item_id_field, has_node_button, spacing=33, margins=33), confirm_add_item)]
        ThemeField: item_id_field:
            text << flow_key(app.app_state_flow_id)
            minimum_size << (app.app_state_win_rectangle[2] * 0.69, app.app_state_font_size * 1.5)
        ThemeButton: has_node_button:
            attr want_node: bool = 'node' in main_app.item_by_id(flow_key(main_app.flow_id))
            tool_tip = "will have node"
            background << enaml_rgba(*(app.app_state_flow_path_ink if self.want_node else (.6, .6, .6, .6)))
            icon_size = int(app.app_state_font_size * 1.5)
            icon = main_app.cached_icon('enter_item', app.app_state_font_size, app.app_state_light_theme)
            minimum_size << (int(app.app_state_font_size * 3), int(app.app_state_font_size * 1.8))
            clicked :: self.want_node = not self.want_node     # does not work w/o self.
        FlowButton: confirm_add_item:
            tap_flow_id << id_of_flow('save', 'item', flow_key(app.app_state_flow_id))
            tap_kwargs << dict(item_popup=edit_item_popup,
                               new_id=item_id_field.text, want_node=has_node_button.want_node)


enamldef ItemDeletionConfirmPopup(FlowPopup): del_item_popup:
    attr node_only: bool
    attr prev_popups_to_close: tuple = ()
    font << str(int(app.app_state_font_size)) + 'px arial'
    ThemeContainer:
        border = Border()
        #constraints = [vbox(hbox(, spacing=33, margins=33), confirm_delete_item)]
        Html:
            minimum_size << (app.app_state_win_rectangle[2] * 0.69, app.app_state_win_rectangle[3] * 0.69)
            source = f"Confirm deletion of item(s):<p/><p/>" \
                + f"{'<p/>'.join(main_app.sub_item_ids(flow_key(main_app.flow_id), node_only))}"
        FlowButton: confirm_delete_item:
            tap_flow_id << id_of_flow('delete', 'item', flow_key(app.app_state_flow_id))
            tap_kwargs << dict(popups_to_close=prev_popups_to_close + (del_item_popup, ), node_only=node_only)


enamldef FlowPathJumperPopup(FlowPopup): flow_path_jumper:
    ThemeContainer:
        Looper:
            iterable = main_app.flow_path_quick_jump_nodes()
            FlowButton:
                tap_flow_id = id_of_flow('jump', 'node', loop.item)
                tap_kwargs = dict(popups_to_close=(flow_path_jumper, ))
                text = loop.item
                minimum_size = (parent.width.value(), main_app.font_size)
