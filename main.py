""" Lisz main module implemented via the Enaml framework.

The documentation and other implementations of this application
you find in the portion/package :mod:`ae.lisz_app_data`
of the ae namespace.
"""
from typing import cast, Any, Callable, Optional, Tuple, Type

import atom.api
import enaml
from ae.gui_app import APP_STATE_SECTION_NAME, replace_flow_action
from enaml.widgets.popup_view import PopupView as PopupsRegister
from enaml.widgets.widget import Widget
with enaml.imports():
    # noinspection PyUnresolvedReferences
    from enaml.stdlib.message_box import MessageBox

from ae.files import CachedFile
from ae.gui_app import id_of_flow, flow_action, flow_key
from ae.enaml_app import EnamlMainApp, FrameworkApp
from ae.lisz_app_data import FLOW_PATH_ROOT_ID, FLOW_PATH_TEXT_SEP, LiszDataMixin, LiszNode

with enaml.imports():
    # noinspection PyUnresolvedReferences
    from main_view import (
        ListItemAtom, ItemDeletionConfirmPopup, ItemPopup as ItemAddPopup, ItemPopup as ItemEditPopup,
        FlowPathJumperPopup)
    # ItemAddPopup/ItemEditPopup not used here, import found by ae.inspector.stack_var()/ae.gui_app.on_flow_change()


__version__ = '0.2.64'


class LiszFrameworkApp(FrameworkApp):
    """ framework app class. """
    app_state_filter_selected = atom.api.Bool()     #: True for to hide selected items
    app_state_filter_unselected = atom.api.Bool()   #: True for to hide unselected items

    filtered_atoms = atom.api.ContainerList()       #: filtered atom observables of :attr:`~LiszApp.current_node_items`


class LiszApp(LiszDataMixin, EnamlMainApp):
    """ app class """
    def _init_default_user_cfg_vars(self):
        """ add/initialize user-specific app states of this app. """
        super()._init_default_user_cfg_vars()
        self.user_specific_cfg_vars |= {
            (APP_STATE_SECTION_NAME, 'filter_selected'),
            (APP_STATE_SECTION_NAME, 'filter_unselected'),
            (APP_STATE_SECTION_NAME, 'root_node'),
        }

    # finally not needed - crashing also with this workaround (enaml focus handling is the culprit, not stack_var())
    # @staticmethod
    # def class_by_name(class_name: str) -> Optional[Type]:
    #     """ overwritten for to prevent usage of stack_var()/inspector module.
    #
    #     unfortunately this is not fixing the crashes.
    #     """
    #     print('CLASS_BY_NAME', class_name, class_name in globals(), globals())
    # Either using globals() to get the class by name:
    #     return globals().get(class_name)
    # Alternative solution (violating DRY):
    #     return dict(ItemAddPopup=ItemAddPopup, ItemEditPopup=ItemEditPopup, ...).get(class_name)

    def display_error(self, err_msg: str):
        """

        :param err_msg:
        :return:
        """
        self.play_beep()
        self.framework_win.status_bar_.show_message(err_msg)

    def dropping_item(self, dragged_node: LiszNode, dragged_item_id: str, drop_item_id: str):
        """ event handler after dropping dragged item.

        :param dragged_node:            node where the item got dragged from.
        :param dragged_item_id:         item id of dragged item.
        :param drop_item_id:            item id of item to drop onto or `FLOW_PATH_ROOT_ID` if dropped onto leave item
                                        button or item id prefixed with `FLOW_PATH_TEXT_SEP` of the sub node item
                                        to drop into.
        """
        self.dpo(f"LiszApp.dropping_item {dragged_node}.{dragged_item_id} to {self.current_node_items}.{drop_item_id}")

        drop_node_path = None
        child_item_id = ''
        if drop_item_id == FLOW_PATH_ROOT_ID:
            drop_node_path = self.flow_path[:-1]
            drop_item_id = ''
        elif drop_item_id.startswith(FLOW_PATH_TEXT_SEP):
            child_item_id = drop_item_id[len(FLOW_PATH_TEXT_SEP):]
            drop_node_path = self.flow_path + [id_of_flow('enter', 'item', child_item_id), ]
            drop_item_id = ''

        refresh = True
        if self.move_item(dragged_node, dragged_item_id, dropped_path=drop_node_path, dropped_id=drop_item_id):
            self.play_sound('edited')
            self.save_app_states()
            if child_item_id or drop_item_id:
                self.change_flow(id_of_flow('focus', 'item', child_item_id or dragged_item_id))
                refresh = False     # on_flow_id() with 'focus' does already a refresh/redraw
        else:
            self.display_error(f"item {dragged_item_id} already exists in node {self.flow_path_text(drop_node_path)}")
        if refresh:
            self.refresh_all()

    def focus_neighbour_sel_button(self, direction: str, ewi: Widget) -> Optional[Widget]:
        """ prev/next neighbour focus child event handler of selection toggle button. """
        if not ewi:
            return None

        row_boxes: list = self.framework_win.scroller.children[0].children
        if len(row_boxes) <= 1:     # empty list - contains only the looper
            return None

        row_idx = row_boxes.index(ewi.parent) if ewi.parent in row_boxes else -1
        if direction == 'prev':
            if row_idx == 0:
                row_idx = -2        # switch focus to last visible row container (last row box is the looper)
            else:
                row_idx -= 1
        else:
            if row_idx == len(row_boxes) - 2:
                row_idx = 0
            else:
                row_idx += 1
        child = row_boxes[row_idx].children[0]
        self.dpo(f"LiszApp.focus_neighbour_sel_button '{getattr(child, 'tool_tip', child)}' {self._refreshing_data}")
        return child

    def focused_sel_button(self, item_id: str):
        """ focus_gained event handler of selection toggle button.

        :param item_id:     list item id of focused select button.
        """
        self.dpo(f"LiszApp.focused_sel_button({item_id}): flow='{self.flow_id}'; refreshing={self._refreshing_data}")
        if not self._refreshing_data:
            self.change_flow(id_of_flow('focus', 'item', item_id))

    def init_app(self, framework_app_class: Type[FrameworkApp] = LiszFrameworkApp) -> Tuple[Callable, Callable]:
        """ inject extended framework app class for to initialize current list data and framework app instance """
        setattr(self, 'on_key_press_of__+', self.on_key_press_of__a)
        setattr(self, 'on_key_press_of__-', self.on_key_press_of__del)
        return super().init_app(framework_app_class=framework_app_class)

    def on_app_start(self):
        """ start app. """
        self.refresh_current_node_items_from_flow_path()
        super().on_app_start()
        self.refresh_all()

    def on_flow_id(self):
        """ refresh screen on flow id change. """
        self.refresh_all()
        self.save_app_states()

    def on_flow_path(self):
        """ enable/disable leave node button on flow path change. """
        self.framework_win.leave_node_button.enabled = self.flow_path_action() == 'enter'

    def on_help_show(self, key: str, event_kwargs: dict) -> bool:
        """ show help screen. """
        self.dpo(f"LiszApp.on_help_show({key} {event_kwargs})")
        mb = MessageBox(text="Sorry No Help Texts Available - add ae_gui_help portion", title=self.app_title,
                        image=cast(CachedFile, self.find_image('app_icon')).loaded_object.images[0].image)
        mb.observe('finished', lambda *_: self.change_flow(id_of_flow('')))
        mb.show()
        return True

    def on_item_delete(self, item_id: str, event_kwargs: dict) -> bool:
        """ delete list item. """
        self.dpo(f"LiszApp.on_item_delete({item_id} {event_kwargs})")
        node_only = event_kwargs.get('node_only', False)
        self.delete_items(item_id, node_only=node_only)
        self.save_app_states()
        event_kwargs['flow_id'] = id_of_flow('focus', 'item', item_id)
        return True

    def on_item_save(self, item_id: str, event_kwargs: dict) -> bool:
        """ update list item. """
        self.dpo(f"LiszApp.on_item_save('{item_id}, {event_kwargs})")

        add_item = (item_id == '')
        item_idx = -1 if add_item else self.find_item_index(item_id)
        item_popup = event_kwargs.pop('item_popup', None)

        action_or_err = self.edit_validate(item_idx, **event_kwargs)

        if action_or_err:
            if action_or_err.startswith('request_delete_confirmation'):
                saved = not self.change_flow(id_of_flow('confirm', 'item_deletion', item_id),
                                             popup_kwargs=dict(prev_popups_to_close=(item_popup, ),
                                                               node_only=action_or_err.endswith('_for_node')))
            else:
                self.display_error(action_or_err)
                saved = False
        else:
            event_kwargs['flow_id'] = id_of_flow('focus', 'item', self.current_node_items[max(item_idx, 0)]['id'])
            if item_popup:
                event_kwargs['popups_to_close'] = (item_popup, )
            self.save_app_states()
            saved = True

        return saved

    def on_key_press_of__a(self) -> bool:
        """ [A] key event handler. """
        if flow_action(self.flow_id) in ('', 'focus'):
            self.change_flow(id_of_flow('add', 'item'))
            return True

    def on_key_press_of__del(self) -> bool:
        """ [Del] key event handler. """
        if flow_action(self.flow_id) == 'focus':
            self.change_flow(id_of_flow('confirm', 'item_deletion', flow_key(self.flow_id)))
            return True

    def on_key_press_of__e(self) -> bool:
        """ [E] key event handler. """
        if flow_action(self.flow_id) == 'focus':
            self.change_flow(replace_flow_action(self.flow_id, 'edit'))
            return True

    def on_key_press_of__enter(self) -> bool:
        """ [Enter/Return] hot key event handler. """
        if flow_action(self.flow_id) == 'focus':
            nid = self.item_by_id(flow_key(self.flow_id))
            if 'node' in nid:
                self.change_flow(id_of_flow('enter', 'item', nid['id']))
                return True

    # on_key_press_of__right = on_key_press_of__enter   right-arrow not propagated by Qt (used for widget focus change)

    def on_key_press_of__escape(self) -> bool:
        """ [Esc] hot key event handler. """
        if self.flow_path:
            flo = id_of_flow('leave', 'item') if self.flow_path_action() == 'enter' else \
                id_of_flow('close', 'flow_popup')
            self.change_flow(flo)
        elif PopupsRegister.popup_views:
            # for pu in reversed(PopupsRegister.popup_views):
            #    pu.close()
            PopupsRegister.popup_views[-1].close()
        else:
            self.stop_app()
        return True

    def refresh_node_widgets(self):
        """ refresh displayed/filtered item indexes and widgets from the current node.

        iterable get directly updated via self.framework_app.filtered_atoms ContainerList.
        """
        self.dpo(f"LiszApp.refresh_node_widgets: flow='{self.flow_id}' path={self.flow_path}")
        cni = self.current_node_items
        unfiltered = not self.filter_selected and not self.filter_unselected
        match_val = 0 if self.filter_selected else 1
        fii = [idx for idx, nid in enumerate(cni) if unfiltered or nid.get('sel', 0) == match_val]
        self.framework_app.filtered_atoms = [ListItemAtom(index=idx, **cni[idx]) for idx in fii]
        self.filtered_indexes = fii

    def widget_by_flow_id(self, flow_id: str) -> Optional[Any]:
        """ determine the list item widget referenced by the passed flow_id.

        :param flow_id:         flow id referencing the focused widget.
        :return:                widget that has the focus when the passed flow id is set.
        """
        item_id = flow_key(flow_id)
        row_boxes: list = self.framework_win.scroller.children[0].children
        for box in row_boxes[:-1]:
            liw = box.children[0]
            if liw.text == item_id:
                return liw
        return super().widget_by_flow_id(flow_id)


# app start
if __name__ == '__main__':
    LiszApp(app_name='lisz', app_title="Irmi's Shopping Lisz").run_app()
