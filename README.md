# enaml lisz

enaml lisz is a Python multi-platform application demo project based on the
[__enaml__ Framework](https://enaml.readthedocs.io) 
and the [__ae__ (Application Environment)](https://ae.readthedocs.io "ae on rtd")
namespace package.

additional credits to:

* [__Erokia__](https://freesound.org/people/Erokia/) and 
  [__plasterbrain__](https://freesound.org/people/plasterbrain/) at
  [freesound.org](https://freesound.org) for the sounds.
* [__iconmostr__](https://iconmonstr.com/interface/) for the icon images.
